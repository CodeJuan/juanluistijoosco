export * from './ping.controller';
export * from './usuario.controller';
export * from './autor.controller';
export * from './libro.controller';
export * from './ejemplar.controller';
export * from './prestamo.controller';
