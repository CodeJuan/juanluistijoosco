import {Entity, model, property} from '@loopback/repository';

@model()
export class Prestamo extends Entity {
  @property({
    type: 'date',
    required: true,
  })
  fecha_dev: string;

  @property({
    type: 'date',
    required: true,
  })
  fecha_prest: string;


  constructor(data?: Partial<Prestamo>) {
    super(data);
  }
}

export interface PrestamoRelations {
  // describe navigational properties here
}

export type PrestamoWithRelations = Prestamo & PrestamoRelations;
