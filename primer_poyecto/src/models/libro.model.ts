import {Entity, model, property} from '@loopback/repository';

@model()
export class Libro extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: false,
    required: true,
  })
  codigo_libro: number;

  @property({
    type: 'string',
    required: true,
  })
  titulo: string;

  @property({
    type: 'string',
    required: true,
  })
  editorial: string;

  @property({
    type: 'string',
    required: true,
  })
  paginas: string;

  @property({
    type: 'string',
    required: true,
  })
  isbn: string;


  constructor(data?: Partial<Libro>) {
    super(data);
  }
}

export interface LibroRelations {
  // describe navigational properties here
}

export type LibroWithRelations = Libro & LibroRelations;
