import {Entity, model, property} from '@loopback/repository';

@model()
export class Ejemplar extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: false,
    required: true,
  })
  codigo: number;

  @property({
    type: 'string',
    required: true,
  })
  localizacion: string;


  constructor(data?: Partial<Ejemplar>) {
    super(data);
  }
}

export interface EjemplarRelations {
  // describe navigational properties here
}

export type EjemplarWithRelations = Ejemplar & EjemplarRelations;
