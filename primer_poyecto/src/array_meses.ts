let meses = [
    { name: 'OCTUBRE', value: 10 },
    { name: 'MAYO', value: 5 },
    { name: 'JULIO', value: 7 },
    { name: 'DICIEMBRE', value: 12 },
    { name: 'ENERO', value: 1 },
    { name: 'MARZO', value: 3 },
    { name: 'AGOSTO', value: 8 },
    { name: 'NOVIEMBRE', value: 11 },
    { name: 'SEPTIEMBRE', value: 9 },
    { name: 'ABRIL', value: 4 },
    { name: 'FEBRERO', value: 2 },
    { name: 'JUNIO', value: 6 },
  ];
  
  console.log('VECTOR OBJETO ORIGINAL: ',meses);
  
  meses.sort(function (a, b) {
    if (a.value > b.value) {
      return 1;
    }
    if (a.value < b.value) {
      return -1;
    }
  
    return 0;
  });
  console.log('VECTOR OBJETO ORDENADO : ',meses.sort());
  let max=meses.length
  console.log('ORDENADO SOLO MOSTRANDO EL NOMBRE: ');
  for (let i = 0; i < max; i++) {
  console.log('--->',meses[i].name);
  }
  