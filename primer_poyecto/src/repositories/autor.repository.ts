import {DefaultCrudRepository} from '@loopback/repository';
import {Autor, AutorRelations} from '../models';
import {PostgresDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AutorRepository extends DefaultCrudRepository<
  Autor,
  typeof Autor.prototype.Codigo,
  AutorRelations
> {
  constructor(
    @inject('datasources.postgresDB') dataSource: PostgresDbDataSource,
  ) {
    super(Autor, dataSource);
  }
}
