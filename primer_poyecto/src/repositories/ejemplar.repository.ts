import {DefaultCrudRepository} from '@loopback/repository';
import {Ejemplar, EjemplarRelations} from '../models';
import {PostgresDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class EjemplarRepository extends DefaultCrudRepository<
  Ejemplar,
  typeof Ejemplar.prototype.codigo,
  EjemplarRelations
> {
  constructor(
    @inject('datasources.postgresDB') dataSource: PostgresDbDataSource,
  ) {
    super(Ejemplar, dataSource);
  }
}
