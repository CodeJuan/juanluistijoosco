export * from './autor.repository';
export * from './ejemplar.repository';
export * from './libro.repository';
export * from './prestamo.repository';
export * from './usuario.repository';
