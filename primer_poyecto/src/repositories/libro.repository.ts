import {DefaultCrudRepository} from '@loopback/repository';
import {Libro, LibroRelations} from '../models';
import {PostgresDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LibroRepository extends DefaultCrudRepository<
  Libro,
  typeof Libro.prototype.codigo_libro,
  LibroRelations
> {
  constructor(
    @inject('datasources.postgresDB') dataSource: PostgresDbDataSource,
  ) {
    super(Libro, dataSource);
  }
}
